import React, {Component} from 'react';
import {observer} from 'mobx-react'
import './App.css';
import MainScreen from './MainScreen.js'
import Choose from './Choose.js'
import Sequence from './Sequence'
import Loot from './Loot'
import {getAllAvailableActions} from './index'

class App extends Component {
    render() {

        let scene = null
        if (this.props.store.scene === 'choose') scene = <Choose />
        if (this.props.store.scene === 'sequence' && this.props.store.actionsQueue.length > 0) {
            const action = getAllAvailableActions().find(a => a.title === this.props.store.actionsQueue[0])
            scene = <Sequence action={action}/>
        }
        if (this.props.store.scene === 'game') scene = <MainScreen />
        if (scene == null)  scene = <Choose />

        return (
            <div className="App bgc-b">
                {scene}
                <Loot />
            </div>
        );
    }
}

export default observer(['store', 'actions'], App);
