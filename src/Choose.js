import React, {Component} from 'react';
import fullscreen from 'screenfull'
import {observer} from 'mobx-react'
import './App.css';

import {humanInitialRessources, robotInitialRessources, getAllAvailableActionsFor, actions} from './index'

import Action from './cmps/Action'
import Ressource from './cmps/Ressource'

class MainScreen extends Component {

    componentDidMount(){
        this.props.store.actionsDone.clear()
    }

    render() {

        const humanActions = getAllAvailableActionsFor('human', false).filter(a => a.sequence != null)
        const robotActions = getAllAvailableActionsFor('robot', false).filter(a => a.sequence != null)
        const totalR = actions.robot.filter(a => a.sequence != null).length
        const progressR = actions.robot.filter(a => this.props.store.keys.indexOf(a.dismiss) > -1).length
        const totalH = actions.human.filter(a => a.sequence != null).length
        const progressH = actions.human.filter(a => this.props.store.keys.indexOf(a.dismiss) > -1).length

        return (
            <div className="" style={{
                backgroundImage   : 'url(TITLE_SCREEN.png)',
                backgroundRepeat  : 'no-repeat',
                backgroundPosition: 'center center',
                backgroundSize    : 'contain',
                width             : '100%',
                height            : '100%'
            }}>
                <button className="m-2 p-2 d-ibl t-c" style={{position: 'absolute', left: 0, bottom: 0, zIndex: 1}} onClick={() => {
                    this.props.store.keys.clear()
                    this.props.store.actionsBatch.clear()
                    this.props.store.actionsDone.clear()
                    this.props.store.actionsQueue.clear()
                }}>
                    Réinitialiser
                </button>

                {fullscreen.enabled && <button className="m-2 p-2 d-ibl t-c" style={{position: 'absolute', left: 160, bottom: 0, zIndex: 1}} onClick={() => {
                    fullscreen.toggle()
                }}>
                    Plein écran
                </button>}
                <button className="humanButton" disabled={humanActions.length === 0 ? 'disabled' : null} onClick={() => {
                    this.props.store.playAs = 'human'
                    this.props.store.ressources = humanInitialRessources.map(r => r)
                    this.props.store.actionsBatch = getAllAvailableActionsFor('human', false).map(a => a.title)
                    this.props.store.scene = 'game'

                    // TODO : supprimer cette ligne quand la partie jeu sera débloquée
                    this.props.store.actionsQueue.push(...humanActions.map(a => a.title))
                    this.props.store.scene = 'sequence'

                }}>
                    <div className="p-2 d-ibl t-c button pos-abs right-0 top-0">
                        Explorer l'esprit <br/><strong className="fs-2">Humain</strong><br/> {totalH === progressH ? 'Terminé !' : ` (${progressH} / ${totalH})`}
                    </div>
                </button>
                <button className="p-2 d-ibl t-c robotButton" disabled={robotActions.length === 0 ? 'disabled' : null} onClick={() => {
                    this.props.store.playAs = 'robot'
                    this.props.store.ressources = robotInitialRessources.map(r => r)
                    this.props.store.actionsBatch = getAllAvailableActionsFor('robot', false).map(a => a.title)
                    this.props.store.scene = 'game'

                    // TODO : supprimer cette ligne quand la partie jeu sera débloquée
                    this.props.store.actionsQueue.push(...robotActions.map(a => a.title))
                    this.props.store.scene = 'sequence'
                }}>
                    <div className="p-2 d-ibl t-c button pos-abs left-0 top-0">
                        Explorer l'esprit <br/><strong className="fs-2">Robot</strong><br/> {totalR === progressR ? 'Terminé !' : ` (${progressR} / ${totalR})`}
                    </div>
                </button>
                <div className="credits bgc-g pos-abs p-1"  style={{left: 0, bottom: 0, color: 'white', width: '100%'}} >
                    <div style={{marginLeft: 250, textAlign: 'right'}}>Utopiales 2016 - Evolution(s)<br/> Réalisé par Jaqueline&nbsp;Florencio&nbsp;(Graph), Kévin&nbsp;Pieplu&nbsp;(Graph),<br/>Nicholas&nbsp;Codet&nbsp;(Graph), Samuel&nbsp;Bouchet&nbsp;(Dev) et Youri&nbsp;Bossus&nbsp;(Sound&nbsp;Design)</div>
                </div>
            </div>
        );
    }
}

export default observer(['store'], MainScreen);
