import React, {Component} from 'react';
import {observer} from 'mobx-react'
import './App.css';

import Ressource from './cmps/Ressource'

class Loot extends Component {
    render() {
        const ll = this.props.store.lastLoot
        const opacity = ll.length === 0 ? 0 : 1
        const width = ll.length === 0 ? '0' : '100%'

        const texts = this.props.store.lastLoot.filter(l => typeof l === 'string')
        const ress = this.props.store.lastLoot.filter(l => typeof l !== 'string')

        const lootBySource = {}
        ress.forEach(loot => {
            if (lootBySource[loot.source] == null) lootBySource[loot.source] = []
            lootBySource[loot.source].push(loot.ressource)
        })

        return (
            <div className="p-2 bgc-g bgo-0p5 pos-abs" style={{
                zIndex    : 100,
                opacity   : opacity,
                width     : width,
                height    : '100%',
                top       : 0,
                left      : 0,
                transition: 'opacity 500ms',
            }}>
                {texts.length > 0 && (
                    <div className="pos-abs bgc-w" style={{left: '50%', top: '50%', transform: `translate(-50%, -50%)`}}>

                        <div className="p-2">
                            <h3>Évolution(s)</h3>
                            <ul>
                                {texts.map((txt, i) => {
                                    return <li key={i}>{txt}</li>
                                })}
                            </ul>
                        </div>
                        {ress.length > 0 && <div className="p-2">
                            <h3>Idées débloquées</h3>
                            {Object.keys(lootBySource).map((key, i) => (
                                <div>
                                    <h4>Pour avoir compris "{key}"</h4>
                                    {lootBySource[key].map(resId => {
                                        const ress = this.props.store.ressources.find(r => r.id === resId)
                                        return ress && <Ressource key={ress.id} {...ress}/>
                                    })}
                                </div>
                            ))}
                        </div>
                        }
                        <button className="p-2 m-2" onClick={(e) => {
                            e.preventDefault()
                            e.stopPropagation()
                            this.props.store.lastLoot.clear()
                        }}>Ok !
                        </button>
                    </div>

                )}
            </div>
        );
    }
}

export default observer(['store'], Loot);
