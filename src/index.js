import React from 'react';
import ReactDOM from 'react-dom';
import {observable, reaction, toJSON, autorun} from 'mobx'
import {Provider} from 'mobx-react'
import App from './App';
import './index.css';
import ee from 'event-emitter'

import robotActions from './actions/robotActions'
import humanActions from './actions/humanActions'

Object.values = x =>
  Object.keys(x).reduce((y, z) =>
  y.push(x[z]) && y, []);

Object.entries = x =>
  Object.keys(x).reduce((y, z) =>
  y.push([z, x[z]]) && y, []);


let iRessource = 0
export function ressource(title, c, l, e, f) {
  return ({
    id: iRessource++,
    title,
    in: null,
    c,
    e,
    l,
    f
  })
}

// TODO : jouer en tant qu'humain / robot
// TODO : glisser déposer
// TODO :

const keys = []

export const robotInitialRessources = [
  ressource('Test', 5, 5, 5, 5),
  ressource('Reconnaissance faciale', 0, 1, 1, 0),
  ressource('Inférence cognitive', 0, 2, 0, 0),
  ressource('Surcharge de l\'horloge', 0, 1, 1, 1),
  ressource('Analyse évolutive', 1, 1, 0, 0),
  ressource('Rasterization énergétique', 0, 1, 0, 2),
  //ressource('Module de simulation des émotions', 0, 2, 0, 0),
  //ressource('Meta-Algorithme', 2, 0, 0, 0),
]

export const humanInitialRessources = [
  ressource('Test', 5, 5, 5, 5),
  ressource('Compétence de reconnaissance faciale', 0, 1, 1, 0),
  ressource('Action du cerveau gauche', 0, 0, 2, 0),
  ressource('Effet de la cafféine', 0, 0, 1, 1),
  ressource('Sentiment étrange', 1, 1, 0, 0),
  ressource('Montée d\'adrénaline', 0, 0, 0, 2),
  //ressource('Emotion prenante', 0, 2, 0, 0),
  //ressource('Réflexion sur soi', 2, 0, 0, 0),
]

const ressources = []

const usedRessources = []

export const actions = {
  robot: robotActions,
  human: humanActions
}

const store = observable({
  ressources,
  usedRessources,
  actionsDone: [],
  actionsBatch: [],
  actionsQueue: [],
  keys,
  scene: 'choose',
  playAs: null,
  emitter: ee(),
  lastLoot: [],
})

export function playSequence(action) {
  store.actionsQueue.push(action.title)
  store.scene = 'sequence'
}

export function completeAction(action, ending) {
  if (ending == null && typeof action.output === 'function') action.output(store, action)
  else if (ending != null && typeof action.output[ending] === 'function') {
    action.output[ending](store, action)
  }
  store.actionsQueue = store.actionsQueue.filter(aTitle => aTitle !== action.title)
  store.actionsDone.push(action.title)
}
export function failAction(action) {
  store.lastLoot.push('Vos choix n\'ont pas permis au personnage de progresser, la séquence "' + action.title + '" sera à nouveau disponible lors de votre prochaine tentative.')
}
export function successAction(action) {
  store.keys.push(action.title)
  store.lastLoot.push('Félicitations, vous avez débloqué des nouvelles séquences ! Explorez à nouveau l\'esprit humain ou robot pour y accéder' )
}

export function getRessources(actionTitle) {
  return store.ressources.filter(res => res.in === actionTitle)
}
export function getAllAvailableRessources() {
  return store.ressources.filter(r => store.usedRessources.indexOf(r.id) === -1)
}
export function getAllAvailableUnaffectedRessources() {
  return getAllAvailableRessources().filter((res, i) => res.in === null && i < 5)
}
export function getAllUsedRessources() {
  return store.ressources.filter(res => store.usedRessources.indexOf(res.id) === -1)
}
export function getAllAvailableActions(sessionOnly = true) {
  return getAllAvailableActionsFor(store.playAs, sessionOnly)
}

export function getAllAvailableActionsFor(target, sessionOnly = false) {
  return actions[target].filter(act => (
    store.actionsDone.indexOf(act.title) === -1
    && (!sessionOnly || store.actionsBatch.indexOf(act.title)) > -1)
    && (act.dismiss == null || store.keys.indexOf(act.dismiss) === -1)
    && (act.locks == null || act.locks.every(l => store.keys.indexOf(l) > -1)))
}

export function getActiveAction() {
  if (store.actionsQueue.length === 0) return null
  return actions[store.playAs].find(a => a.title === store.actionsQueue[0])
}

export function validateActions(actions, ressources) {
  store.lastLoot.clear()
  actions.forEach(a => {
    if (isComplete(a)) {
      if (a.sequence != null) {
        playSequence(a)
      } else {
        completeAction(a)
      }
    } else {
      store.actionsDone.push(a.title)
    }
  })
  store.ressources.filter(r => r.in != null).forEach(r => {
    r.in = null
    store.usedRessources.push(r.id)
  })
  //console.log(toJSON(store))
}

export function isComplete(action) {
  return 'clef'.split('').map(value => isValueComplete(action, value)).every(isOk => isOk)
}

function isValueComplete(action, v) {
  const ressValues = getRessources(action.title).map(r => r[v])
  const summ = ressValues.reduce((sum, val)=> sum + val, 0)
  return action.cost[v] <= summ
}

export function addRessource(store, ressource, source) {
  store.ressources.push(ressource)
  store.lastLoot.push({source, ressource: ressource.id})
}

function tryGotoChoose(){
  if (store.playAs == null || store.scene == 'choose' || store.actionsQueue.length > 0) return
  const actions = getAllAvailableActions()
  if (actions.length === 0) {
    store.scene = 'choose'
    store.actionsDone.clear()
    store.usedRessources.clear()
  }
}
autorun(tryGotoChoose);

ReactDOM.render(
  <Provider store={store} actions={actions}><App /></Provider>,
  document.getElementById('root')
);