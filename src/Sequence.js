import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {observer} from 'mobx-react'
import './App.css';
import Sound from './libs/Sound'

import {completeAction, failAction, getActiveAction} from './index'

const initialState = {
    step         : 0,
    prevDetail   : null,
    repeatStep   : 0,
    viewedAnswers: []
}

class Sequence extends Component {

    constructor() {
        super()
        this.state = initialState
    }

    componentWillUnmount() {
        this.state = initialState
    }

    answer(a, i) {
        if (a.next != null) {
            this.setState({
                step         : (typeof a.next != 'string' ? a.next : this.state.step + 1),
                prevDetail   : (typeof a.next != 'string' ? '' : a.next) + (typeof a.header != 'string' ? '' : a.header),
                repeatStep   : 0,
                viewedAnswers: []
            })
        }
        else if (a.repeat != null) {
            if (typeof a.repeat !== 'string' && this.state.repeatStep >= a.repeat.length) {
                completeAction(getActiveAction(), a.over)
                return
            }
            this.setState({
                prevDetail   : (typeof a.repeat === 'string' ? a.repeat : a.repeat[this.state.repeatStep]),
                repeatStep   : this.state.repeatStep + 1,
                viewedAnswers: (typeof a.repeat === 'string' ? [...this.state.viewedAnswers, i] : this.state.viewedAnswers)
            })
        }
        else if (a.over != null) {
            completeAction(getActiveAction(), a.over)
        }

        if (a.repeat == null) {
            const action = getActiveAction()
            if (action != null && this.state.step + 1 >= action.sequence.length) {
                completeAction(action, a.over)
            }
        }
    }

    render() {
        const action = getActiveAction()
        if (action == null) return null
        const seq = action.sequence[this.state.step]
        return (
            <div className="pos-rel bgc-b" style={{
                backgroundImage   : 'url(' + seq.bg + ')',
                backgroundRepeat  : 'no-repeat',
                backgroundPosition: 'center center',
                backgroundSize    : 'contain',
                width             : '100%',
                height            : '100%'
            }}>
                <Sound
                    url={action.music}
                    loops={99999}
                    playStatus={Sound.status.PLAYING}
                />

                <ReactCSSTransitionGroup transitionName="o" transitionAppear={true} transitionAppearTimeout={500}
                                         transitionLeave={false}
                                         transitionEnterTimeout={500} transitionEnter={true}>
                    <div key={this.state.step + '-' + this.state.prevDetail} className="textLine">
                        {this.state.prevDetail && <span>{this.state.prevDetail} </span>}
                        <span>{seq.line}</span>
                    </div>
                    <div key={this.state.step + '-z' + this.state.prevDetail} className="tc-w">
                        <span className="p-2" style={{float: 'left'}}>{action.title}</span>
                    </div>
                    <div key={this.state.step + '-2' + this.state.prevDetail} className="pos-abs" style={{bottom: 50, right: 50}}>
                        <ul>
                            {seq.answers
                                .map((a, i) => (
                                    (this.state.viewedAnswers.indexOf(i) === -1 && (!a.locks || a.locks.every(lock => this.state.viewedAnswers.indexOf(lock) > -1)))) && (
                                    <li key={i}>
                                        <button className="p-2" onClick={() => this.answer(a, i)}
                                                style={{width: '100%', backgroundColor: 'rgba(10,10,10,0.4)'}}>
                                            {a.line}
                                        </button>
                                    </li>
                                ))}
                        </ul>

                    </div>
                </ReactCSSTransitionGroup>
            </div>
        );
    }
}

export default observer(['store'], Sequence);
