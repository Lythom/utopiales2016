import React from 'react'
import {ressource, failAction, getActiveAction, successAction} from '../index'

export const rencontres = {
    title   : 'Rencontres',
    cost    : ressource(null, 2, 2, 1, 2),
    music: 'Robot/1 - La pelle laser/Robot - thème.mp3',
    locks   : ['Robotéisme', 'Maintenant'],
    sequence: [ // line, answers, output
        {
            line   : ['- Merci pour ton aide robot, tu peux maintenant entrer en phase de veille.'],
            bg     : 'Robotéisme.png',
            answers: [
                {line: '(Robot) [Sortir de l\'église]', next: ''},
            ]
        },
        {
            line   : ['[Robot et Humain entrent en collision]'],
            bg     : 'bousculade.png',
            answers: [
                {line: '(Robot) Je vous prie de m\'excuser', next: '« Je vous prie de m\'excuser »'},
                {line: "(Robot) [Regarder l'humain avec stupéfaction]", over: 'fail', repeat: ['Et bien alors ?', 'Tu bugs ?', 'Les machines… Éternelles mais sans intelligence !']},
                {line: '(Robot) Pourquoi faites-vous ça ?', next: '« Pourquoi faites-vous ça ? »'},
            ]
        },
        {
            line   : [''],
            bg     : 'bousculade.png',
            answers: [
                {line: '(Humain) Fais donc attention !', over: 'success'},
                {line: '(Humain) Qu… Occupe-toi de tes affaires !', over: 'success'},
                {line: '(Humain) [Ne pas répondre]', over: 'success'},
            ]
        }
    ],
    dismiss : 'Rencontres',
    output  : {
        success: (store) => {
            successAction(getActiveAction())
        },
        fail   : () => {
            failAction(getActiveAction())
        },
    }
}

export const decision = {
    title   : 'Décision',
    cost    : ressource(null, 2, 2, 1, 2),
    music: 'Robot/5 - Confrontation/Confrontation.mp3',
    locks   : ['Changer', 'Epreuve'],
    sequence: [ // line, answers, output
        { // 0
            line   : '(Robot) - Pourquoi ? POURQUOI ?',
            bg     : 'FINAL_1.png',
            answers: [
                {line: '(Humain) Je peux répondre à cette question.', next: 1},
                {line: "(Humain) Laisse-le s'expliquer !", next: 2, 'header': '[Le robot relâche le prêtre] (Prêtre) '},
                {line: '(Humain) [Laisser faire le robot]', over:'pretremort'},
            ]
        },
        { // 01
            line   : '(Robot) - Vous feriez mieux de vous dépêcher.',
            bg     : 'FINAL_1.png',
            answers: [
                {line: '(Humain) Cet homme essaye de me sauver. Il a dû faire pareil pour vous.', next: "[Le robot relâche le prêtre] (Prêtre) C'est exactement ça."},
                {line: '(Humain) Cet homme t\'as construit, il est ton maître. Relâche-le à présent.', next: "[Le robot relâche le prêtre] (Prêtre) Pas du tout !"},
                {line: '(Humain) Non en fait je ne sais pas, Laisse-le s\'expliquer.', next: "[Le robot relâche le prêtre] (Prêtre) "},
                {line: '(Humain) [Laisser faire le robot]', over:'pretremort'},
            ]
        },
        { // 02
            line   : [" Les morts ne peuvent plus rien faire pour se racheter… Je donne des deuxièmes chances.", <br />,
                "Le corps que vous avez enterré est le vôtre, et celà vous a aidé à reprendre vos esprits dans ce nouveau corps. Votre partie robotique réintègre déjà peu à peu vos sentiments humains."],
            bg     : 'LABORATOIRE.png',
            answers: [
                {line: '(Robot) J\'étais… humain ? …Le suis-je encore ?', next: "(Prêtre) Humaine pour être exact. Il vous appartient de reconstruire votre humanité éthique, je vous aiderai au mieux dans cette tâche."},
                {line: "(Robot) [Refuser l'explication et éliminer le prêtre]", over:'pretremortconfirm'},
            ]
        },
        { // 03
            line   : "",
            bg     : 'LABORATOIRE.png',
            answers: [
                {line: '(Humain) Q… Quoi ? Je préfère mourir que de risquer mon humanité !', repeat: "(Humain) « Je préfère mourir que de risquer mon humanité ! »"},
                {line: '(Robot) Moi aussi ! [Étrangler le prêtre]', locks: [0],  over:'pretremortconfirm'},
                {line: '(Robot) Je pense être encore humain. Si ça peut changer quelque chose.', locks: [0], next:'(Robot) « Je pense être encore humain. Si ça peut changer quelque chose. »'},
            ]
        },
        { // 04
            line   : "",
            bg     : 'LABORATOIRE.png',
            answers: [
                {line: '(Humain) Très bien, essayons.', over: 'success'},
                {line: '(Humain) Faites comme bon vous semble, je préfère accepter ma mort !', over: 'tolerance'},
                {line: "(Humain) Un robot ne peut être humain ! [Éliminer le robot]", next:5},
            ]
        },
        { // 05
            line   : "!",
            bg     : 'FINAL_3.png',
            answers: [
                {line: '…', over: 'robotdead'},
            ]
        }
    ],
    dismiss : 'theend',
    output  : {
        tolerance: (store) => {
            store.keys.push('theend')
            store.lastLoot.push("Est-ce mourir que de changer de forme ? L'immortalité d'un corps ne vaudrait-elle pas la peine de prendre ce risque face à une fatalité imminante ? Merci d\'avoir joué ! Nous espérons que le jeu vous a plu, si le coeur vous en dit venez nous trouvez pour en discuter !")
        },
        robotdead: (store) => {
            store.keys.push('theend')
            store.lastLoot.push("Avez-vous détruit une machine ou tué un être humain ? Êtes-vous sûr ? Merci d\'avoir joué ! Nous espérons que le jeu vous a plu, si le coeur vous en dit venez nous trouvez pour en discuter !")
        },
        pretremort: (store) => {
            store.lastLoot.push("Un peu extrême… N'est-il pas plus sage de comprendre avant de prendre une vie ?")
        },
        pretremortconfirm: (store) => {
            store.keys.push('theend')
            store.lastLoot.push("Jouer avec la vie serait dangereux pour l'humanité ? Votre choix était-il éthiquement humain ? Merci d\'avoir joué ! Nous espérons que le jeu vous a plu, si le coeur vous en dit venez nous trouvez pour en discuter !")
        },
        success: (store) => {
            store.keys.push('theend')
            store.lastLoot.push("Vous acceptez la possibilité qu\'une machine soit humaine ? La définition biologique de l\'humain n'est-elle pas nécessaire pour définir l'Homme ? Merci d\'avoir joué ! Nous espérons que le jeu vous a plu, si le coeur vous en dit venez nous trouvez pour en discuter !")
        },
        fail   : () => {
            failAction(getActiveAction())
        },
    }
}