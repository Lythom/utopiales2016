import React from 'react'
import {ressource, addRessource, failAction, getActiveAction, successAction} from '../index'
import {decision, rencontres} from './commonActions'
import Sound from '../libs/Sound'

export default [
    {
        title : 'Accident de voiture',
        cost  : ressource(null, 0, 0, 2, 0),
        output: (store, act) => {
            addRessource(store, ressource('Prévention', 0, 2, 2, 0), act.title)
        }
    },
    {
        title : 'Livre philosophique : "Bonheur robotique ?"',
        cost  : ressource(null, 2, 0, 0, 0),
        output: (store, act) => {
            addRessource(store, ressource('Remise en question', 3, 0, 1, 0), act.title)
        }
    },
    {
        title : 'Prière à l\'Homme',
        cost  : ressource(null, 1, 1, 1, 1),
        output: (store, act) => {
            addRessource(store, ressource('Foi robotique', 1, 2, 2, 1), act.title)
            addRessource(store, ressource('Paradoxe : preuve de l\'inexistance', 2, 1, 1, 2), act.title)
        }
    },
    {
        title : 'Article : déséquilibres sociaux-économiques post-technomodernes',
        cost  : ressource(null, 0, 4, 0, 0),
        output: (store, act) => {
            addRessource(store, ressource('Algorithmes de répartition égalitaire', 1, 5, 2, 0), act.title)
        }
    },
    {
        title : 'Séquence historique des sciences humaines',
        cost  : ressource(null, 1, 2, 2, 0),
        output: (store, act) => {
            addRessource(store, ressource('Routine de traitement des bio-organismes', 2, 4, 4, 2))
        }
    },
    {
        title : 'Composants logistiques',
        cost  : ressource(null, 2, 0, 2, 0),
        output: (store, act) => {
            addRessource(store, ressource('Analyse du sentiment humain', 3, 0, 3, 0))
            addRessource(store, ressource('Analyse du sentiment animal', 1, 0, 1, 0))
        }
    },
    {
        title : 'Séquence historique des sciences médicales 2',
        cost  : ressource(null, 0, 1, 0, 1),
        output: (store, act) => {
            addRessource(store, ressource('Routine de traitement des bio-organismes', 3, 3, 0, 0))
        }
    },
    {
        title : 'Composants militaires 2',
        cost  : ressource(null, 0, 0, 0, 2),
        output: (store, act) => {
            addRessource(store, ressource('Système export de neutralisation des individus', 0, 0, 0, 4))
            addRessource(store, ressource('Algorithmes d\'analyse de destruction génie civil', 0, 0, 2, 2))
        }
    },
    {
        title   : 'Robotéisme',
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'Robot/1 - La pelle laser/Robot - thème.mp3',
        sequence: [ // line, answers, output
            {
                line   : 'Apporte moi la pelle laser !',
                bg     : 'Robotéisme.png',
                answers: [ // line, repeat, next, over
                    {line: 'Oui, Mon Père !', next: 'Bien !'},
                    {line: 'Pourquoi avoir besoin d\'une pelle dans une église ?', repeat: 'Pour creuser un trou pardi !'},
                    {
                        line  : '[Continuer d\'observer]',
                        repeat: ['Et bien alors ?', 'Tu te réveilles ?', 'Oh non il est encore buggué !', 'Je vais devoir le désactiver…'],
                        over  : 'fail'
                    },
                    {line: 'Ou puis-je trouver la pelle ?', next: 'Utilise ton module de localisation enfin !'},
                ]
            },
            {
                line   : 'Maintenant, creuse !',
                bg     : 'Robotéismeb.png',
                answers: [
                    {line: 'Oui, Mon Père !', next: ''},
                    {line: 'Pourquoi creuser dans ce cimetière ?', repeat: 'Pour enterrer… quelque chose. Aller au boulot !'},
                    {
                        line  : '[Continuer d\'observer]',
                        repeat: ['Et bien alors ?', 'Tu te réveilles ?', 'Oh non il est encore buggué !', 'Je vais devoir le désactiver…'],
                        over  : 'fail'
                    },
                    {line: 'Merci de spécifier les dimensions du trou attendu', repeat: 'Assez grand pour enterrer… ça.'}
                    // TODO : creuser une pelletée
                    // TODO : mimétisme
                ]
            },
            {
                line   : ['(Robot) - Application de la règle 42.c chapitre 2. Est-ce un bras humain ?', <br/>,
                    '(Prêtre) - Non. Enterre-le à présent.'],
                bg     : 'Robotéismeb.png',
                answers: [
                    {line: '[Procéder]', over: 'fail'},
                    {line: '[Remttre en doute et analyser le bras]', over: 'success'},
                    // todo: creuser une pelleté
                ]
            }
        ],
        dismiss : 'Robotéisme',
        output  : {
            success: (store, act) => {
                successAction(getActiveAction())
            },
            fail   : () => {
                failAction(getActiveAction())
            },
        }
    },
    Object.assign({}, rencontres, {}),
    {
        title   : 'Suspicion',
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'Robot/3 - Suspicion et fuite/Suspicion et fuite.mp3',
        locks   : ['Rencontres'],
        sequence: [ // line, answers, output
            {
                line   : '(Plus tard…) Je dois lever l\'ambiguïté de cette information.',
                bg     : 'suspicion.png',
                answers: [
                    {line: '[Déterrer le corps]', next: '[Creuse]'},
                    {line: "[S'enfuir]", over: 'fail'},
                ]
            },
            {
                line   : ['C\'est… Impossible !', <Sound url="Robot/2 - Le cimetière/Bruitages/creuxer pelle2.mp3" volume={50} playStatus={Sound.status.PLAYING}/>],
                bg     : 'suspicion.png',
                answers: [
                    {line: "[S'enfuir]", over: 'success'},
                ]
            }
        ],
        dismiss : 'Suspicion',
        output  : {
            success: (store, act) => {
                successAction(getActiveAction())
            },
            fail   : () => {
                failAction(getActiveAction())
            },
        }
    },
    {
        title   : 'Epreuve',
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'Robot3 - Suspicion et fuite/Fuite.mp3',
        locks   : ['Suspicion'],
        sequence: [ // line, answers, output
            {
                line   : '[Cours]',
                bg     : 'fuite.png',
                answers: [
                    {line: "Je dois le retrouver avant qu'il ne recommence !", next: 'Utilisation du module de localisation.'},
                    {line: "Je dois m'enfuir", over: 'fail'},
                ]
            },
            {
                line   : "C'est ici.",
                bg     : 'affiche-robot.png',
                answers: [
                    {line: "Je dois le retrouver.", over: 'success'},
                    {line: "Danger détecté. Stratégie de repli.", over: 'fail'},
                ]
            }
        ],
        dismiss : 'Epreuve',
        output  : {
            success: (store, act) => {
                successAction(getActiveAction())
                store.lastLoot.push('Si vous ne l\'avez pas encore réalisée, la séquence humain "Changer" sera nécessaire pour débloquer la suite de l\'histoire')
            },
            fail   : () => {
                failAction(getActiveAction())
            },
        }
    },
    Object.assign({}, decision, {

    }),

    /*
     1 service
     2 creuser
     3 enterre
     4. bousculade (robot3) 3-3
     5. suspicion : retourne voir le cadavre
     6. fuite
     7. labo ending 5 -6

     1 nouvelle
     2 rue
     3 boulsculade 3b bar
     4 confession
     5 transhumanisme affiche
     6 labo 6-5
     */
]