import React from 'react'
import {ressource, addRessource, failAction, getActiveAction, successAction} from '../index'
import {decision, rencontres} from './commonActions'

export default [
    {
        title : 'Accident de voiture',
        cost  : ressource(null, 0, 0, 2, 0),
        output: (store) => {
            addRessource(store, ressource('Prudence', 1, 1, 2, 0), this.title)
        }
    },
    {
        title : 'Livre philosophique : "Bonheur robotique ?"',
        cost  : ressource(null, 2, 0, 0, 0),
        output: (store) => {
            addRessource(store, ressource('Raisonnement par l\'absurde', 2, 1, 1, 0), this.title)
        }
    },
    {
        title : 'Prière à Dieu',
        cost  : ressource(null, 1, 1, 1, 1),
        output: (store) => {
            addRessource(store, ressource('Espoir', 0, 0, 3, 1), this.title)
            addRessource(store, ressource('Foi humaine', 0, 0, 1, 3), this.title)
        }
    },
    {
        title : 'Article : déséquilibres sociaux-économiques post-technomodernes',
        cost  : ressource(null, 0, 4, 0, 0),
        output: (store) => {
            addRessource(store, ressource('Injustice', 0, 2, 2, 0), this.title)
            addRessource(store, ressource('Volonté de changement', 0, 2, 0, 2), this.title)
        }
    },
    {
        title : 'Séquence historique des sciences humaines',
        cost  : ressource(null, 1, 2, 2, 0),
        output: (store) => {
            addRessource(store, ressource('Ecoute de soi', 2, 4, 4, 2))
        }
    },
    {
        title : 'Composants logistiques',
        cost  : ressource(null, 2, 0, 2, 0),
        output: (store) => {
            addRessource(store, ressource('Empathie humaine', 3, 0, 3, 0))
            addRessource(store, ressource('Empathie animale (miaou !)', 1, 0, 1, 0))
        }
    },
    {
        title : 'Séquence historique des sciences médicales 2',
        cost  : ressource(null, 0, 1, 0, 1),
        output: (store) => {
            addRessource(store, ressource('Appréhension des corps', 3, 3, 0, 0))
        }
    },
    {
        title : 'Composants militaires 2',
        cost  : ressource(null, 0, 0, 0, 2),
        output: (store) => {
            addRessource(store, ressource('Musculature', 0, 0, 0, 4))
            addRessource(store, ressource('Utilise ta tête !', 0, 0, 2, 2))
        }
    },
    {
        title   : 'Faire face',
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'humain/Humain climax.mp3',
        sequence: [ // line, answers, output
            {
                line   : '… il vous reste donc à vivre entre 1 et 2 mois tout au plus.',
                bg     : 'med.png',
                answers: [
                    {line: '[Pleurer]', next: 2},
                    {line: 'N\'y a t\'il rien que je puisse faire ?', next: 'Je ne pense pas.'},
                    {line: 'Aidez-moi s\'il vous plaît.', next: 'La seule chose que je puisse vous proposer est la procédure d\'accompagnement.'},
                    {line: 'Je vois…', next: 1},
                ]
            },
            {
                line   : ['Malgré les progrès fulgurants de la néo-médecine, la dégénérescence des tissus internes ne peut être contrée.', <br />,
                    'Une greffe rallonge en moyenne l\'espérance de vie de 2 semaines, la liste d\'attente effective est de 2 ans.'],
                bg     : 'med.png',
                answers: [
                    {line: '[Pleurer]', next: 2},
                    {line: 'Il doit-y avoir un moyen !', next: 3},
                    {line: 'Il me reste des choses à accomplir. Je suis prêt à tout.', next: 3},
                    {line: '[Partir en courant]', over: 'fail'},
                ]
            },
            {
                line   : 'Courage monsieur.',
                bg     : 'med.png',
                answers: [
                    {
                        line  : '[Pleurer]',
                        repeat: ['La procédure d\'accompagnement vous permettra de tenir cette épreuve.', 'Je vais appeler ma collègue pour votre suivi.', '[Numérotation en cours]', 'Au revoir monsieur.']
                    },
                    {line: 'Il doit-y avoir un moyen !', next: 3},
                    {line: 'Il me reste des choses à accomplir. Je suis prêt à tout.', next: 3},
                    {line: '[Partir en courant]', over: 'fail'},
                ]
            },
            {
                line   : 'Il y a peut être un piste… mais le résultat pourrait être pire que la mort. Je vous recommande vivement de suivre notre procédure d\'accompagnement qui vous apportera le plus grand réconfort.',
                bg     : 'med.png',
                answers: [
                    {line: ['Merci.', <i><br />(Il ne semble pas vouloir en dire plus)</i>], over: 'success'},
                ]
            }
        ],
        dismiss : 'Faire face',
        output  : {
            success: (store) => {
                successAction(getActiveAction())
            },
            fail   : () => {
                failAction(getActiveAction())
            },
        }
    },
    {
        title   : 'Maintenant',
        locks   : ['Faire face'],
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'humain/Humain climax.mp3',
        sequence: [ // line, answers, output
            {
                line   : [<br/>, 'Et maintenant ?'],
                bg     : 'rue.png',
                answers: [
                    {line: 'Faire le bilan sur mon existance ?', repeat: 'Oui, me confier à quelq\'un pourrait me faire du bien.'},
                    {line: 'Boire un verre ?', repeat: 'Ce n\'est pas ça qui me tuera de toute façon, maintenant c\'est sûr.'},
                    {line: 'Appeler Camille ?', repeat: 'Camille ne sera pas rentré de son déplacement de toute façon. Autant préserver son insouciance pour le moment.'},
                    {line: 'Aller de l\'avant', over: 'success', locks: [0, 1, 2]},
                    {line: 'Fuir ?', repeat: ['Est-ce vraiment une solution ?', 'Je me sens tellement perdu…'], over: 'fail'},
                ]
            }
        ],
        dismiss : 'Maintenant',
        output  : {
            success: (store) => {
                successAction(getActiveAction())
            },
            fail   : () => {
                failAction(getActiveAction())
            },
        }
    },
    {
        title   : 'Faire face autrement',
        locks   : ['Maintenant'],
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'humain/Humain climax.mp3',
        sequence: [ // line, answers, output
            {
                line   : ['(Humain) - Un Vin-blanc-Limonade-Grenadine, s\il vous plaît.', <br/>, '(Barman) - Le "Jaqueline" ?', <br/>, "(Femme) - On ne me commande pas !"],
                bg     : 'Bar.png',
                answers: [
                    {line: '?!', next: 'Je m\'appelle Jaqueline, enchantée. Et pour la peine je vous offre le cocktail. Vous avez l\'air d\'en avoir besoin.'},
                    {line: 'Vous vous appelez Jaqueline ?', next: 'On ne peut rien vous cacher ! Pour la peine je vous offre le cocktail. Vous avez l\'air d\'en avoir besoin'},
                    {line: 'Je commande si je veux !', next: 2, header: "Rustre ! Hahaha, je n'ai jamais vu une approche aussi peu délicate !"},
                    {line: 'C\'était une mauvais idée de venir', over: 'fail'},
                ]
            },
            {
                line   : "",
                bg     : 'Bar.png',
                answers: [
                    {line: "Ce n'est rien de le dire…", repeat: 'Oh… pauvre chou. Trinquons à votre moral alors.'},
                    {line: "Malheureusement, le plus à propos serait à ma santé plutôt", locks: [0], repeat: 'Je vois… Alors à votre santé !'},
                    {line: 'Vous vous appelez vraiment Jaqueline ?', repeat: "Ce n'est plus aussi fréquent que ça à pu l'être mais c'est bien mon nom."},
                    {line: 'En effet c\'est désuet…', locks: [2], next: "Rustre ! Hahaha, je n'ai jamais vu une approche aussi peu délicate !"},
                    {line: '[Sortir du bar]', over: 'fail'},
                ]
            },
            {
                line   : "",
                bg     : 'Bar.png',
                answers: [
                    {line: "Il m'a plutôt semblé que c'est vous qui approchiez…", repeat: 'Ça vous ennui ?'},
                    {line: "C'est plutôt plaisant", locks: [0], next: 'Et ce n\'est que le début…'},
                    {line: "Mais je n'ai pas vraiment le moral", locks: [0], next: 'J\'ai peut être une solution…'},
                    {line: '[Sortir du bar]', over: 'fail'},
                ]
            },
            {
                line   : "",
                bg     : 'Bar.png',
                answers: [
                    {line: "[S'approcher de Jaqueline]", over: 'success'},
                    {line: '[S\'enfuir]', over: 'fail'},
                ]
            }
        ],
        dismiss : 'Faire face autrement',
        output  : {
            success: (store) => {
                store.keys.push('Faire face autrement')
                store.lastLoot.push("Le bonheur ne se trouve t'il pas parfois sur le chemin plutôt que dans la destination ?")
            },
            fail   : (store) => {
                store.keys.push('Faire face autrement')
                store.lastLoot.push("C'est tout ? Très bien, allons droit au but.")
            },
        }
    },
    Object.assign({}, rencontres, {
    })
    ,
    {
        title   : 'Confession',
        locks   : ['Rencontres'],
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'humain/Humain climax.mp3',
        sequence: [ // line, answers, output
            {
                line   : "- Bonjour mon fils. Confiez-vous avec justesse et vous serez Entendu.",
                bg     : 'confession.png',
                answers: [
                    {line: 'Je vais mourir… je ne suis pas prêt', next: '… c\'est tragique en effet. Si vous vous sentez capable de changer profondément, de l\'aide vous sera apportée. [Le prêtre vous donne une note avec une adresse griffonnée.]'},
                    {line: 'Je n\'ai pas toujours été un modèle…', next: '… c\'est tragique en effet. Si vous vous sentez capable de changer profondément, de l\'aide vous sera apportée. [Le prêtre vous donne une note avec une adresse griffonnée.]'},
                    {line: 'Je pense mériter mieux…', next: '… c\'est tragique en effet. Si vous vous sentez capable de changer profondément, de l\'aide vous sera apportée. [Le prêtre vous donne une note avec une adresse griffonnée.]'},
                    {line: '[S\'en aller]', over: 'fail'},
                ]
            },
            {
                line   : '',
                bg     : 'confession.png',
                answers: [
                    {line: 'Changer… Serais-je encore moi-même ?', repeat: 'Est-ce que je sais seulement qui je suis ?'},
                    {line: 'Changer… A quel prix ?', repeat: 'Souffrance ? Sentiments ? Argents ? Proche ? Que suis-je prêt à perdre pour continuer de vivre ?'},
                    {line: 'Changer… Et vivre encore combien de temps ?', repeat: 'L\'immortalité ? Celà semble trop beau.'},
                    {line: 'Changer.', locks:[0,1,2], over: 'success'},
                    {line: '[s\'en aller]', over: 'fail'},
                ]
            }
        ],
        dismiss : 'Confession',
        output  : {
            success: (store) => {
                successAction(getActiveAction())
            },
            fail   : () => {
                failAction(getActiveAction())
            },
        }
    },
    {
        title   : 'Changer',
        locks   : ['Confession'],
        cost    : ressource(null, 2, 2, 1, 2),
        music   : 'humain/Humain climax.mp3',
        sequence: [ // line, answers, output
            {
                line   : <i>Arrivé à l'adresse</i>,
                bg     : 'affiche-humain.png',
                answers: [
                    {line: 'Est-ce vraiment la solution ?', repeat: ''},
                    {line: 'Ai-je seulement le choix ?', locks: [0], repeat: ''},
                    {line: 'Revoir Camille ainsi, ou ne jamais revoir Camille…', locks: [1], repeat: ''},
                    {line: 'C\'est maintenant ou jamais.', locks: [2], over:'success'},
                    {line: "Wow ! Changer oui, mais pas comme ça !", over: 'fail'},
                ]
            }
        ],
        dismiss : 'Changer',
        output  : {
            success: (store) => {
                successAction(getActiveAction())
                store.lastLoot.push('Si vous ne l\'avez pas encore réalisée, la séquence robot "Epreuve" sera nécessaire pour débloquer la suite de l\'histoire')
            },
            fail   : () => {
                failAction(getActiveAction())
            },
        }
    },
    Object.assign({}, decision, {

    })
    ,
]