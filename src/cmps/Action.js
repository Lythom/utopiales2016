import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import {observer} from 'mobx-react'
import '../App.css';
import Value from './Value'
import Ressource from './Ressource'
import throttle from 'lodash.throttle'
import {getRessources, isComplete} from '../index'

class Action extends Component {

    constructor() {
        super()
        this.isM = false
        this.state = {}
        this.stopDragB = this.stopDrag.bind(this)
        this.handleRessourceAtB = throttle((a, b) => this.handleRessourceAt(a, b), 200)
    }

    componentDidMount() {
        this.isM = true
        this.props.store.emitter.on('ressourceAt', this.handleRessourceAtB)
        window.addEventListener('mouseup', this.stopDragB)
        window.addEventListener('touchend', this.stopDragB)
    }

    componentWillUnmount() {
        this.isM = false
        this.props.store.emitter.off('ressourceAt', this.handleRessourceAtB)
        window.removeEventListener('mouseup', this.stopDragB)
        window.removeEventListener('touchend', this.stopDragB)
    }

    stopDrag(e) {
        if (!this.state.highlighted || this.lastRessourceId == null) return

        const ress = this.props.store.ressources.find(r => r.id === this.lastRessourceId)
        if (ress != null) ress.in = this.props.action.title
        this.setState({
            highlighted: false
        })
    }

    handleRessourceAt(ressource, pos) {
        if (!this.isM) return
        const ress = this.props.store.ressources.find(r => r.id === ressource)
        if (ress == null) return

        this.lastRessourceId = ressource
        const ref = ReactDOM.findDOMNode(this.refs.handle);
        const box = ref.getBoundingClientRect();
        if (box.left < pos.x && box.right > pos.x && box.top < pos.y && box.bottom > pos.y) {
            this.setState({
                highlighted: true
            })
        } else {
            this.setState({
                highlighted: false
            })
        }
    }

    valueOf(v) {
        const ressValues = getRessources(this.props.title).map(r => r[v])
        const summ = ressValues.length === 0 ? 0 : ressValues.reduce((sum, val)=> sum + val, 0)
    }

    render() {
        const ress = getRessources(this.props.action.title)
        let color = ''
        if (this.state.highlighted) color = 'rgba(58,76,41,0.9)'
        const complete = isComplete(this.props.action)
        if (complete) color = 'rgba(65,95,58,0.9)'


        const cValue = ress.reduce((total, r) => total + r.c, 0)
        const eValue = ress.reduce((total, r) => total + r.e, 0)
        const lValue = ress.reduce((total, r) => total + r.l, 0)
        const fValue = ress.reduce((total, r) => total + r.f, 0)

        return (
            <div className="pos-abs top-0">
                <div ref="handle"
                     className={'tc-w mr-1 p-1 bgc-g d-ibl pos-abs card' + (this.props.action.sequence != null ? ' epic' : '')}
                     style={{
                         backgroundColor   : color,
                         backgroundImage   : 'url(Carte.png)',
                         backgroundRepeat  : 'no-repeat',
                         backgroundPosition: 'center center',
                         transform         : `translate(${this.props.offsetX}px, ${this.props.offsetY}px)`,
                         width             : 171,
                         height            : 244,
                     }}>
                    <div className="pb-1 t-b" style={{color: color}}>{this.props.action.title}</div>
                    <ul className="pos-abs" style={{bottom:10, left: 10}}>
                        <Value type="c" max={this.props.action.cost.c } value={cValue}/>
                        <Value type="l" max={this.props.action.cost.l } value={lValue}/>
                        <Value type="e" max={this.props.action.cost.e} value={eValue}/>
                        <Value type="f" max={this.props.action.cost.f} value={fValue}/>
                    </ul>
                </div>
            </div>
        );
    }
}

Action.propTypes = {
    title: React.PropTypes.string,
    c    : React.PropTypes.number,
    l    : React.PropTypes.number,
    f    : React.PropTypes.number,
    e    : React.PropTypes.number,
}

export default observer(['store', 'actions'], Action);
