import React, {Component} from 'react';
import '../App.css';

class Action extends Component {

    label() {
        switch (this.props.type) {
            case 'c':
                return 'Conscience'
            case 'l':
                return 'Logique'
            case 'e' :
                return 'Empatie'
            case 'f':
                return 'Force d\'esprit'
            default:
                return ''
        }
    }

    img(c) {
        switch (this.props.type) {
            case 'c':
                return <CIcon color={c}/>
            case 'l':
                return <LIcon color={c}/>
            case 'f':
                return <FIcon color={c}/>
            case 'e' :
                return <EIcon color={c}/>
            default:
                return <img src={this.props.type + ".svg"} width="20" height="20"/>
        }
    }

    render() {
        const isComplete = this.props.value >= this.props.max
        return (
            <li className="mw-120 ta-l" style={{height: 25}} title={this.label()}>
                <span className={`d-ibl br-50p h-value valign-bot`} style={{marginRight: 8, width: 10}} key={-1}>{this.props.type.toUpperCase()}</span>
                <span className={`d-ibl valign-bot pos-abs`} style={{width:200, marginLeft:-1,marginTop:-4}}>{Array.from({length: this.props.max}, (_, i) =><span className={`d-ibl bgc-w br-50p h-value w-value m-2px border-4`} key={i}>{this.img('#999999')}</span>)}</span>
                <span className={`d-ibl valign-bot pos-abs`} style={{width:200,marginTop:-4}}>
                    {Array.from({length: this.props.value}, (_, i) =><span className={`d-ibl bgc-v${this.props.type} br-50p h-value w-value m-2px`} key={i}>{this.img(isComplete ? '#000000' : '#000000')}</span>)}
                </span>
            </li>
        );
    }
}

Action.propTypes = {
    type : React.PropTypes.oneOf(['c', 'l', 'f', 'e']),
    value: React.PropTypes.number,
}

export default Action;

function CIcon({color = '#010101', width = 20, height = 20}) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44.8 68.1" style={{
            width,
            height
        }}>
            <style type="text/css" dangerouslySetInnerHTML={{
                __html: `
            .st0{fill:none;}
            .st1{fill:${color};}
            .st2{fill:#FFFFFF;stroke:${color};stroke-miterlimit:10;}
`
            }}>
            </style>
            <path class="st1" style={{fill: color}}
                  d="M44.7 18.3C44.1 9.3 33.8-14 5 11.7 4.6 12-0.2 21.4 0 23c0.2 1.2 0.5 2.4 2.1 4.1 1.7 1.7 0.7 3.1 0.5 5.7 -0.2 3.2 2.4 4.9 3.2 7.7C6.6 43.1 5.9 45.5 8 51c0 0 2.3 6.5 3.1 9.6 0.8 3.1 6.1 6.6 7.6 7.2 1.6 0.6 5 0.6 6.8-1 1.6-1.4 5.2-5.1 7.9-4.3 2.5-3.2 5.5-5.9 9-8C41.7 49 45.1 23.4 44.7 18.3zM9.5 40c-2 0.2-3.7-1.4-3.9-3.6 -0.2-2.2 1.3-4 3.2-4.2 2-0.2 3.7 1.4 3.9 3.6C13 37.9 11.5 39.8 9.5 40zM23.4 38.8c-2 0.2-3.7-1.4-3.9-3.6 -0.2-2.2 1.3-4 3.2-4.2 2-0.2 3.7 1.4 3.9 3.6C26.9 36.7 25.4 38.6 23.4 38.8z"/>
            <path class="st2" style={{fill: '#FFFFFF', stroke: color, strokeMiterlimit: 10}}
                  d="M5.8 19c0 0 1.5-9.5 9.4-8.5 0.2 0 0.5 0 0.7-0.1 1.4-0.9 8.8-6.9 11.6-2.4 0 0 5-2.6 9.1 1 0 0 4.4 2.1 3.9 4.3 0 0-2.7 5.4-5 4.3 0 0 1.3 2.4-3.3 4.9 0 0-1.5 5.4-10 5.9 0 0-3.8 5.4-5.7 3.7 0 0-5.5 1.4-6.1-3.5C9.6 23.8 5.7 25.8 5.8 19z"/>
        </svg>
    )
}

function EIcon({color = '#010101', width = 20, height = 20}) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44.8 68.1" style={{
            width,
            height,
            stroke          : '#FFFFFF',
            strokeWidth     : 0.75,
            strokeMiterlimit: 10,
            fill            : color
        }}>

            <path class="st0" d="M7.6 46.3c0 0-6.1-6.2-6.8-11.7s0-18.9 1.9-20.6 8.6-4.4 14.7 0.8S7.6 46.3 7.6 46.3"/>
            <path class="st0" d="M9.6 11.7c-0.6 0 0-10.3 0-10.3h5.5v11.8L9.6 11.7z"/>
            <path class="st0"
                  d="M43.1 13.7c-5.4 1.9-8.8-3.7-14.7-0.6 -5.9 3.1-5.6 10.6-5.6 10.6s2.1 0.4 3.6 0.8c-0.1-1.6 2.1-5 3.1-6.5 0.3-0.5 0.8-0.9 1.3-1 6.2-2.2 6.4-0.3 11.6 1 0.4 0.1 1 0.3 1.8 0.5v-5C43.9 13.5 43.5 13.6 43.1 13.7z"/>
            <path class="st0" d="M27.3 25.3C27.3 25.3 27.3 25.3 27.3 25.3c1.4-0.1 0.5-0.4-0.9-0.8 0 0.2 0.1 0.4 0.2 0.5C26.8 25.1 27.1 25.2 27.3 25.3z"/>
            <path class="st0"
                  d="M36.3 62.8c7.2-13.2 1-41.4 0.3-41.4h-3.3c-1 0-2 0.5-2.6 1.3 -0.9 1.2-0.8 2.8 0.2 3.9 0.9 1 1.8 2.6 2.1 4.7 0.5 3.8 2.8 24 3.4 29.8 0.1 0.2 0.1 0.3 0.2 0.5C36.6 61.8 36.2 62.6 36.3 62.8z"/>
            <path class="st0" d="M21.4 23.9l0-0.1C21.4 23.8 21.4 23.9 21.4 23.9z"/>
            <path class="st0"
                  d="M26.6 25c-4.1-1.6-5-1.5-5.2-1.3l0 0.1c0 0 0-0.1 0-0.1l2.2-7.6c0.4-1.5 1.4-2.7 2.7-3.6l1.3-0.9c0.8-0.5 1.9-0.7 2.8-0.4 2.1 0.6 5.8 1.3 6.5-0.9 0.8-2.4-3.1-5.6-4.7-6.8 -0.5-0.3-1-0.6-1.6-0.6 -5.5-0.7-8.9 2.2-10.1 3.4 -0.3 0.3-0.6 0.5-0.9 0.7 -2.6 1.5-5.1 16-4.1 18C16.9 28 9 42.1 6.6 46.2 6.2 47 6 47.9 6.3 48.7c3.1 12.1 17.2 16.7 26.1 18 2.4 0.4 4.3-1.7 3.9-3.9 -0.1 0.1-0.1 0.2-0.2 0.3 0 0 0.4-1.1 0.3-2 -4.1-11.8-1.2-32.4-7.7-35.2 -0.5-0.2-1-0.4-1.4-0.6C26.9 25.3 26.7 25.2 26.6 25z"/>
            <path class="st0" d="M27.3 25.3c-0.3-0.1-0.5-0.2-0.7-0.3C26.7 25.2 26.9 25.3 27.3 25.3z"/>
            <path class="st0" d="M36.3 62.8c0-0.2 0.4-1 0.3-1.2 -0.1-0.2-0.1-0.3-0.2-0.5 0.1 1-0.3 2-0.3 2C36.1 63 36.2 62.9 36.3 62.8z"/>
        </svg>
    )
}

function FIcon({color = '#010101', width = 20, height = 20}) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44.8 68.1" style={{
            width,
            height,
            stroke          : '#FFFFFF',
            strokeWidth     : 0.75,
            strokeMiterlimit: 10,
            fill            : color
        }}>
            <path
                d="M40.8 22.4L19 17.7c-1-0.2-1.9 0.3-2.3 1.1l-2.1 4.5c-0.5 1.2 0.2 2.5 1.5 2.8l7.9 1.6c1.9 0.4 2.3 2.9 0.5 3.8 -3.9 1.9-8.7 4.3-10.1 5.4L5.3 61.7 21 67.9c1.3 0.5 2.8-0.4 2.9-1.8l1.3-21.9c0-0.7 0.4-1.3 1.1-1.7L40.4 35c0.6-0.3 1-0.9 1.1-1.6l1-8.8C42.5 23.6 41.8 22.7 40.8 22.4z"/>
            <path d="M23.9 31l-10.1-3.6c0 0-1.5-5.2-0.9-7.1l-5.9-1.8c0 0-3.9 41.1-6.3 41.8s3.1 2.6 3.1 2.6L13.3 35 23.9 31z"/>
            <path d="M42.7 21.5c2.5 0.4 2-17.6 2-17.6s-3.3-0.8-5.4 0c-2.1 0.8-3.6 14.9-3.4 15.8S42.7 21.5 42.7 21.5z"/>
            <path d="M13.3 19.8l2.2-11.9c0 0-0.2-4.9-4.3-4.3C7.2 4.2 8.4 18.2 8.4 18.2L13.3 19.8z"/>
            <path d="M20.8 16.6c0 0 5.3-14.5 1.8-16.2s-4.9 3.6-4.9 3.6L15.1 19C16.4 17.7 20.8 16.6 20.8 16.6z"/>
            <line x1="14" y1="17.6" x2="14.2" y2="16.5"/>
            <path d="M35.2 1c-3.1-1-2.9-0.6-6.1 0 -3.3 0.6-3.6 16.4-3.6 16.4l5.8 1.2C31.3 18.6 38.2 2.1 35.2 1z"/>
        </svg>
    )
}

function LIcon({color = '#010101', width = 20, height = 20}) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44.8 68.1" style={{
            width,
            height,
            stroke          : '#FFFFFF',
            strokeWidth     : 0.75,
            strokeMiterlimit: 10,
            fill            : color
        }}>
            <path class="st0" d="M11.5 47.7v14.6c0 1.6 1.6 3 3.5 3h15.7c1.9 0 3.5-1.3 3.5-3V47.3"/>
            <path class="st0" d="M34.2 47.9c6-4.4 10-12.4 10-20.9 0-13.5-9.5-26.4-22-26.4S0.5 12.9 0.5 26.4c0 9.6 3.5 17.5 11 21.4H34.2z"/>
            <path class="st0" d="M29.9 65.3c-6.8 3.8-15 0-15 0"/>
            <line class="st0" x1="14.3" y1="52.1" x2="29.4" y2="52.1"/>
            <line class="st0" x1="14.3" y1="57.3" x2="29.4" y2="57.3"/>
            <polyline class="st0" points="16.9 47.5 16.9 25.4 13.8 23.2 28.6 23.2 25.5 25.4 25.5 47.5 "/>
        </svg>
    )
}



