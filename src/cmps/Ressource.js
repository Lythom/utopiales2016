import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import {observer} from 'mobx-react'
import '../App.css';
import Value from './Value'


class Ressource extends Component {

    constructor() {
        super()
        this.state = {
            isDragging: false,
            pos       : {x: 0, y: 0},
        }

        this.stopDragB = e => this.stopDrag(e)
        this.mouseMoveB = e => this.mouseMove(e)
    }

    componentWillUnmount() {
        this.stopDrag()
    }

    mouseMove(e) {
        if (!this.state.isDragging) return

        this.setState({
            pos: {
                x: e.pageX - this.state.relX,
                y: e.pageY - this.state.relY
            }
        }, () => {
            this.props.store.emitter.emit('ressourceAt', this.props.id, {x: e.pageX, y: e.pageY})
        });


        e.preventDefault()
    }

    startDrag(e) {

        const ref = ReactDOM.findDOMNode(this.refs.handle);
        const body = document.body;
        const box = ref.getBoundingClientRect();
        this.setState({
            relX      : e.pageX + 40,
            relY      : e.pageY + 10,
            isDragging: true,
        });
        window.addEventListener('mouseup', this.stopDragB)
        window.addEventListener('touchend', this.stopDragB)
        window.addEventListener('mousemove', this.mouseMoveB)
        window.addEventListener('touchmove', this.mouseMoveB)
    }

    stopDrag(e) {
        this.setState({
            isDragging: false,
            pos       : {x: 0, y: 0},
        })
        window.removeEventListener('mouseup', this.stopDragB)
        window.removeEventListener('touchend', this.stopDragB)
        window.removeEventListener('mousemove', this.mouseMoveB)
        window.removeEventListener('touchmove', this.mouseMoveB)
    }

    render() {

        return (
            <div ref="handle" className={`tc-w mr-1 bgc-y d-ibl mw-160 mh-160 cur-ptr shiny pos-rel`}
                 onMouseDown={e => this.startDrag(e)}
                 style={{
                     backgroundImage   : 'url(ressource.png)',
                     backgroundRepeat  : 'no-repeat',
                     backgroundPosition: 'center center',
                     height            : 150,
                     width             : 244,
                     padding           : '0.5rem',
                     boxSizing         : 'border-box',
                     borderRadius      : 9,
                     transform         : (this.state.isDragging ? `translate(${this.state.pos.x}px, ${this.state.pos.y}px)` : null),
                     position          : (this.state.isDragging ? 'absolute' : 'relative')
                 }}>
                <div className="pb-1 t-b">{this.props.title}</div>
                <ul className="pos-abs" style={{bottom:5}}>
                    <Value type="c" value={this.props.c || 0}/>
                    <Value type="l" value={this.props.l || 0}/>
                    <Value type="e" value={this.props.e || 0}/>
                    <Value type="f" value={this.props.f || 0}/>
                </ul>
            </div>
        );
    }
}

Ressource.propTypes = {
    c: React.PropTypes.number,
    l: React.PropTypes.number,
    f: React.PropTypes.number,
    e: React.PropTypes.number,
}

export default observer(['store'], Ressource);
