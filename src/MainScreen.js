import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import Sound from './libs/Sound'

import {toJSON} from 'mobx'
import {observer} from 'mobx-react'
import './App.css';
import {
    getAllAvailableUnaffectedRessources,
    getAllAvailableActions,
    getAllAvailableRessources,
    validateActions,
    isComplete
} from './index'

import Action from './cmps/Action'
import Ressource from './cmps/Ressource'

class MainScreen extends Component {

    componentDidMount() {
        this.forceUpdateB = () => this.forceUpdate()
        window.addEventListener('resize', this.forceUpdateB)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.forceUpdateB)
    }

    render() {
        const store = this.props.store
        const ress = getAllAvailableUnaffectedRessources()
        const affectedRess = getAllAvailableRessources().filter((res, i) => res.in != null)
        const actions = getAllAvailableActions().filter((a, i) => i < 4)

        const souvenirsCount = getAllAvailableActions().length - actions.length
        const souvenirs = souvenirsCount == 0 ? '' : `(+${getAllAvailableActions().length - actions.length} dans la pioche)`

        const someActionsCompleted = actions.some(a => isComplete(a))

        return (
            <div className="p-2 tc-w" style={{
                backgroundImage   : 'url(BACKGROUND.png)',
                backgroundRepeat  : 'no-repeat',
                backgroundPosition: 'center center',
                backgroundSize    : 'contain',
                width             : '100%',
                height            : '100%'
            }}>

                <Sound
                    url={'Interface.mp3'}
                    volume={20}
                    loops={99999}
                    playStatus={Sound.status.PLAYING}
                />

                <div className="ta-l pos-abs" style={{left: 350, top: '1rem'}}>
                    <h3>Souvenirs {souvenirs}</h3>
                    <ReactCSSTransitionGroup className="p-2 pos-rel o-h" component="div"
                                             transitionAppear={true} transitionAppearTimeout={1000} transitionName="fromltr"
                                             transitionEnterTimeout={1000} transitionLeaveTimeout={1000}
                                             style={{
                                                 height: 800,
                                                 width : 820,
                                                 left  : 0,
                                                 margin: 'auto'
                                             }}
                    >
                        {actions.map((act, i) => (
                            <Action key={act.title + i} action={act} offsetX={((i * 200) % 400) * (window.innerWidth / 1024)}
                                    offsetY={(Math.floor(i / 2) * 260) * (window.innerHeight / 700)}/>
                        ))}
                    </ReactCSSTransitionGroup>
                </div>

                <div>
                    <h3 style={{
                        transform      : 'rotate(-90deg)',
                        transformOrigin: 'left top 0',
                        position       : 'absolute',
                        top            : '50%',
                        left           : '1rem',
                    }}>Idées (+{getAllAvailableRessources().length - ress.length - affectedRess.length} dans la pioche)</h3>
                    <ReactCSSTransitionGroup component="div" transitionAppear={true} transitionAppearTimeout={1000}
                                             transitionName="fromltr" transitionEnterTimeout={1000} transitionLeave={false}
                                             className="p-2 mh-240 pos-abs top-0" style={{
                        width : 280,
                        height: '100%',
                        left  : '2.5rem',
                    }}>
                        {ress.map((ress, i) => (
                            <div key={ress.id} tabIndex="0" className="pos-abs top-0 hovertop" style={{
                                transform : `translate(${0}px, ${(i * (window.innerHeight / 6))}px)`,
                                outline   : 'none',
                                transition: 'transform 200ms'
                            }}>
                                <Ressource {...ress}/>
                            </div>
                        ))}
                    </ReactCSSTransitionGroup>
                </div>

                <div className="pos-abs" style={{bottom: 50, right: 50}}>
                    <div className="ta-l" style={{width: 260, paddingBottom: '2rem'}}>
                        Confrontez (Glisser/Déposer) les Idées acquises sur les Souvenirs pour aboutir à leur compréhension.
                    </div>
                    <ul>
                        <li>
                            <button className="p-2" style={{width: '100%', backgroundColor: someActionsCompleted ? 'rgba(65,95,58,0.4)' : 'rgba(10,10,10,0.4)'}} onClick={() => {
                                validateActions(actions, ress)
                            }}>{someActionsCompleted ? 'Etudier les souvenirs' : ['Garder ses idées', <br/>, '(Passer ces souvenirs)']}
                            </button>
                        </li>
                        <li>
                            <button className="p-2" disabled={affectedRess.length == 0} style={{width: '100%', backgroundColor: 'rgba(10,10,10,0.4)'}} onClick={() => {
                                this.props.store.ressources.filter(r => r.in != null).forEach(r => {
                                    r.in = null
                                })
                            }}>Reprendre ses idées
                            </button>
                        </li>
                        <li>
                            <button className="p-2" style={{width: '100%', backgroundColor: 'rgba(10,10,10,0.4)'}}
                                    onClick={() => {
                                        store.scene = 'choose'
                                        store.actionsDone.clear()
                                        store.usedRessources.clear()
                                    }}>Abandonner cette étude
                            </button>
                        </li>
                    </ul>

                </div>

            </div>
        );
    }
}

export default observer(['store'], MainScreen);
